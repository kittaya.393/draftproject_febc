let count = 5;

if (true) {
    let count = 10; // This 'count' is scoped to the block and is different from the outer 'count'
    console.log(count); // Output: 10
}

console.log(count); // Output: 5
count = 7; // You can reassign a new value to the 'count' variable
console.log(count); // Output: 7

// program calculate odd/even number
let number = 7;
if (number % 2 === 0) {
    console.log("The number is even");
} else {
    console.log("The number is odd");
}

// pregram count string
let str = "Hello, World!";

// if (str length > 0) {
//     console.log("The string is not empty");
// } else {
//     console.log("The string is empty");
// }

// grater/less than 
let num1 = 42;
let num2 = 13;

if (num1 > num2) {
    console.log("num1 is greater than num2");
} else if (num1 < num2) {
    console.log("num1 is less than num2");
} else {
    console.log("num1 is equal to num2");
}

// check element or data in array
let arr = [1, 2, 3, 4, 5];
let value = 3;

if (arr.indexOf(value) !== -1) {
    console.log("The value is in the array");
} else {
    console.log("The value is not in the array")
}

// plus 2 number
function add(a,b) {
    return a + b;
}
let sum = add(3, 4);
console.log(sum); // Output:7

let name = 'John D'; // 1.variable declaration statement

function greet(person) { // 2.function declaration statement
    return 'Hello, ${person}!';
}

if (name.length > 0) {  // 3.if-else statement
    console.log('Name is not empty');
} else {
    console.log('Name is empty');
}

for (let i = 0; i < 5; i++) {  // 4.for loop statement
    console.log('Iteration ${i}');
}

let counter = 0; // 5.while loop statement
while (counter < 5) {
    console.log('Counter: ${counter}');
    counter++;
}

// example object in object
let students = [
    {
        name: 'John',
        age: 22
        gender: 'lgbtq+',
        address: {
            street: 'Ladphrao',
            city: 'Bangkok',
            country: 'Thailand',
            postalCode: '10230',
        },
        parentName: ['Somsri', 'Somchai'],
        sayHello: function () {
            console.log('Hello, ')
        }
    },
    {
        name: 'Bowr',
        age: 27
        gender: 'lgbtq+',
        address: {
            street: 'Ladphrao',
            city: 'Bangkok',
            country: 'Thailand',
            postalCode: '10230',
        },
        parentName: ['Somsri', 'Somchai'],
        sayHello: function () {
            console.log('Hello, I am' + this.name)
        }
    },
]
document.getElementById('title').style.color = "green"

function changeElementbyId(){
    document.getElementById('title').style.color = "pink"
    document.getElementById('title').style.fontSize = "100px"
    document.getElementById('title').innerHTML = "Text"
    // ลองสลับกัน comment ท่อนบนกับท่อนล่าง
    let titleElement = document.getElementById('title')
    // console.log(titleElement)
    if(titleElement.classList.contains("hidden")) {
        titleElement.classList.remove("hidden")
    } else {
        titleElement.classList.add("hidden")
    }
}

console.log(student.name)